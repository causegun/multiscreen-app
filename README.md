# Multi Screen App


## Task Description

Let's imagine that you need to implement an application with four screens. Each screen should have a title (the title should be unique for each screen) and two buttons: Go Forward and Go Back. By tapping on the Go Forward button, the next screen should be opened. By tapping on the Go Back button, the current screen should be closed and the next screen should be opened.

Also, after reaching the last fourth screen, the Go Forward button should open the first one. Moreover, in this case, you should drop the stack of previous activities, and keep only the first one, so using the Go Backward button on the first screen will close the current screen and the app will be closed.

## Complete the Task

Create a program that meets the requirements from the description above. You need to take the following steps:

- Create each screen with buttons and titles.
- Add listeners for each button.
- Add logs to Logcat for each lifecycle event of each screen.
- Implement the correct navigation mode in case of navigating from the last screen to the first one. Example of the behavior:
- A user launches the app. The user is on the first screen (the stack is empty, tapping Back will close the app).
- The user taps the Go Forward button on the first screen. As a result, the user is on the second screen (the stack contains the first screen, tapping Back will open the first screen).
- The user taps the Go Forward button on the second screen. As a result, the user is on the third screen (the stack contains the first and second screens, tapping Back will open the second screen).
- The user taps the Go Forward on the third screen. As a result, the user is on the fourth screen (the stack contains the first, second, and third screens, tapping Back will open the third screen).
- The user taps the Go Forward button on the fourth screen. As a result, the user is on the first screen (the stack is empty, tapping Back will close the app).
- The user taps the Go Forward button on the first screen. As a result, the user is on the second screen (the stack contains the first screen, tapping Back will open the first screen).
- And so on.