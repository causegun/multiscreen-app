package com.example.multiscreenapp

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class Third: AppCompatActivity() {
    private lateinit var forwardButton : Button
    private lateinit var backButton : Button
    private val activityNumber = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        forwardButton = findViewById(R.id.forwardButton)
        backButton = findViewById(R.id.backButton)
        findViewById<TextView>(R.id.title).text = "Activity $activityNumber"
        forwardButton.setOnClickListener { startActivity(Intent(this, Fourth::class.java)) }
        backButton.setOnClickListener { finish() }
        lifecycle.addObserver(MultiScreenAppLifecycleObserver(activityNumber))
    }

}