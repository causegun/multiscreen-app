package com.example.multiscreenapp

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class Fourth: AppCompatActivity() {
    private lateinit var forwardButton : Button
    private lateinit var backButton : Button
    private val activityNumber = 4

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        forwardButton = findViewById(R.id.forwardButton)
        backButton = findViewById(R.id.backButton)
        findViewById<TextView>(R.id.title).text = "Activity $activityNumber"
        forwardButton.setOnClickListener {
            finishAffinity()
            startActivity(Intent(this, MainActivity::class.java))
        }
        backButton.setOnClickListener { finish() }
        lifecycle.addObserver(MultiScreenAppLifecycleObserver(activityNumber))
    }
}